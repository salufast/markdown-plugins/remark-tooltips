// TypeScript Version: 3.9
import {Plugin} from 'unified'

declare namespace tooltips {
  type Tooltips = Plugin<[]>
}

declare const remarkTooltips: tooltips.Tooltips

export = remarkTooltips
