import {unified} from 'unified'
import tooltips from 'remark-tooltips'

unified().use(tooltips)
