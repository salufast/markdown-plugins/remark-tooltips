import parse from 'remark-parse'
import {test} from 'tape'
import {unified} from 'unified'
import {removePosition} from 'unist-util-remove-position'
import {u} from 'unist-builder'
import tooltips from '../index.js'

test('parse', (t) => {
  const basic = unified().use(parse).use(tooltips)

  t.deepEqual(
    removePosition(basic.parse('[text]{def}'), true),
    u('root', [
      u('paragraph', [
        u(
          'html',
          '<span class="tooltip"><span class="tooltip-text">text</span><span class="tooltip-note">def</span></span>'
        )
      ])
    ]),
    'should not parse inline footnotes by default'
  )
  t.end()
})
