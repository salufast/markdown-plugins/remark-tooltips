import {tooltip} from 'micromark-extension-tooltip'
import {tooltipFromMarkdown} from 'mdast-util-tooltip'

let warningIssued

function tooltips() {
  const data = this.data()

  /* c8 ignore start */
  if (
    !warningIssued &&
    ((this.Parser &&
      this.Parser.prototype &&
      this.Parser.prototype.blockTokenizers) ||
      (this.Compiler &&
        this.Compiler.prototype &&
        this.Compiler.prototype.visitors))
  ) {
    warningIssued = true
    console.warn(
      '[remark-tooltips] Warning: please upgrade to remark 13 to use this plugin'
    )
  }
  /* c8 ignore stop */

  add('micromarkExtensions', tooltip())
  add('fromMarkdownExtensions', tooltipFromMarkdown)

  function add(field, value) {
    /* c8 ignore next */
    if (data[field]) data[field].push(value)
    else data[field] = [value]
  }
}

export default tooltips
